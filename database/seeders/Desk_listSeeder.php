<?php

namespace Database\Seeders;

use App\Models\Desk_list;
use Illuminate\Database\Seeder;

class Desk_listSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Desk_list::factory(100)->create();
    }
}
